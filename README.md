# Conway's Game of Life

This application allows you to interact with the Game of Life by creating an initial configuration and observing how it evolves

## Installation

You can clone this project and then run:

```bash
  npm install
  npm start
```

## Demo

- Desktop

![Desktop Demo](https://im4.ezgif.com/tmp/ezgif-4-96f9a93864.webp)

- And mobile fiendly

![Mobile Demo](https://im4.ezgif.com/tmp/ezgif-4-e7aaa5d4a7.webp)

## Deployment

This project is deployed on netlify and can be found here: https://vibrant-morse-9d4472.netlify.app/

## Tech Stack

**Client:** React JS, TypeScript, Material UI

## Authors

- [@George_K](https://gitlab.com/George_K)
