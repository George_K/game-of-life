import { Container } from "@mui/material";
import React from "react";
import { ApplicationBar } from "./components/application-bar";
import { ApplicationMainPage } from "./pages/main-page";

const App: React.FC = () => {
  return (
    <>
      <ApplicationBar />
      <Container
        maxWidth="md"
        style={{ marginTop: "30px", marginBottom: "30px" }}
      >
        <ApplicationMainPage />
      </Container>
    </>
  );
};

export default App;
