import { Divider, Typography } from "@mui/material";
import { GameOfLife } from "../../components/game-of-life";
import stillLifeImage from "./images-of-examples/still-life.png";
import oscillatorsGif from "./images-of-examples/oscillators.gif";
import spaceshipsGif from "./images-of-examples/spaceships.gif";

export const ApplicationMainPage = (): JSX.Element => {
  const dividerMargin = { marginBottom: "16px", marginTop: "16px" };

  return (
    <>
      <Typography
        color="text.primary"
        variant="h3"
        style={{ fontSize: "1.5em" }}
      >
        Introduction
      </Typography>
      <Typography
        variant="body1"
        color="GrayText"
        style={{ marginBottom: "8px", marginTop: "8px" }}
      >
        The Game of Life, also known simply as Life, is a cellular automaton
        devised by the British mathematician John Horton Conway in 1970. It is a
        zero-player game, meaning that its evolution is determined by its
        initial state, requiring no further input. One interacts with the Game
        of Life by creating an initial configuration and observing how it
        evolves. It is Turing complete and can simulate a universal constructor
        or any other Turing machine.
      </Typography>
      <Divider style={dividerMargin} />

      <Typography
        color="text.primary"
        variant="h3"
        style={{ fontSize: "1.5em" }}
      >
        Rules
      </Typography>
      <Typography
        variant="body1"
        color="GrayText"
        style={{ marginBottom: "8px", marginTop: "8px" }}
      >
        The universe of the Game of Life is an infinite, two-dimensional
        orthogonal grid of square cells, each of which is in one of two possible
        states, live or dead (or populated and unpopulated, respectively). Every
        cell interacts with its eight neighbours, which are the cells that are
        horizontally, vertically, or diagonally adjacent. At each step in time,
        the following transitions occur:
      </Typography>

      <div style={{ color: "#000" }}>
        <ol>
          <li>
            Any live cell with fewer than two live neighbours dies, as if by
            underpopulation.
          </li>
          <li>
            Any live cell with two or three live neighbours lives on to the next
            generation.
          </li>
          <li>
            Any live cell with more than three live neighbours dies, as if by
            overpopulation.
          </li>
          <li>
            Any dead cell with exactly three live neighbours becomes a live
            cell, as if by reproduction.
          </li>
        </ol>
      </div>
      <Divider style={dividerMargin} />

      <Typography
        color="text.primary"
        variant="h3"
        style={{ fontSize: "1.5em" }}
      >
        Try It Yourself
      </Typography>

      <GameOfLife />

      <Divider style={dividerMargin} />

      <Typography
        color="text.primary"
        variant="h3"
        style={{ fontSize: "1.5em" }}
      >
        Examples Of Patterns
      </Typography>

      <Typography
        variant="body1"
        color="GrayText"
        style={{ marginBottom: "8px", marginTop: "8px" }}
      >
        Many different types of patterns occur in the Game of Life, which are
        classified according to their behaviour. Common pattern types include:
        still lifes, which do not change from one generation to the next;
        oscillators, which return to their initial state after a finite number
        of generations; and spaceships, which translate themselves across the
        grid.
      </Typography>

      <div
        style={{
          display: "grid",
          gridTemplateColumns: "auto auto auto",
          textAlign: "center",
        }}
      >
        <div>
          <Typography
            variant="body1"
            color="GrayText"
            style={{
              marginBottom: "8px",
              marginTop: "8px",
              fontWeight: "bold",
            }}
          >
            Still Lifes
          </Typography>

          <img
            src={stillLifeImage}
            alt="grid"
            style={{
              width: "100px",
              height: "100px",
            }}
          />
        </div>
        <div>
          <Typography
            variant="body1"
            color="GrayText"
            style={{
              marginBottom: "8px",
              marginTop: "8px",
              fontWeight: "bold",
            }}
          >
            Oscillators
          </Typography>
          <img
            src={oscillatorsGif}
            alt="grid"
            style={{
              width: "100px",
              height: "100px",
            }}
          />
        </div>
        <div>
          <Typography
            variant="body1"
            color="GrayText"
            style={{
              marginBottom: "8px",
              marginTop: "8px",
              fontWeight: "bold",
            }}
          >
            Spaceships
          </Typography>
          <img
            src={spaceshipsGif}
            alt="grid"
            style={{
              width: "100px",
              height: "100px",
            }}
          />
        </div>
      </div>
    </>
  );
};
