import React, { useCallback, useRef, useState } from "react";
import produce from "immer";
import { Button } from "@mui/material";
import ShuffleIcon from "@mui/icons-material/Shuffle";
import DeleteOutlineOutlinedIcon from "@mui/icons-material/DeleteOutlineOutlined";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import PauseIcon from "@mui/icons-material/Pause";

const NUM_ROWS = 20;
const NUM_COLS = 20;

const OPERATIONS = [
  [0, 1],
  [0, -1],
  [1, -1],
  [-1, 1],
  [1, 1],
  [-1, -1],
  [1, 0],
  [-1, 0],
];

const generateEmptyGrid = () => {
  const rows = [];
  for (let i = 0; i < NUM_ROWS; i++) {
    rows.push(Array.from(Array(NUM_COLS), () => 0));
  }

  return rows;
};

const generateRandomGrid = () => {
  const rows = [];
  for (let i = 0; i < NUM_ROWS; i++) {
    rows.push(Array.from(Array(NUM_COLS), () => (Math.random() > 0.7 ? 1 : 0)));
  }

  return rows;
};

export const GameOfLife = (): JSX.Element => {
  const [running, setRunning] = useState(false);

  const [grid, setGrid] = useState(() => {
    return generateEmptyGrid();
  });

  /**
   * Used ref to keep track of the running state because runSimulation
   * runs many times and cant keep track of running
   */
  const runningRef = useRef(running);
  runningRef.current = running;

  const runSimulation = useCallback(() => {
    if (!runningRef.current) {
      return;
    }

    setGrid((g) => {
      return produce(g, (gridCopy) => {
        for (let i = 0; i < NUM_ROWS; i++) {
          for (let k = 0; k < NUM_COLS; k++) {
            let neighbors = 0;
            OPERATIONS.forEach(([x, y]) => {
              const newI = i + x;
              const newK = k + y;

              if (
                newI >= 0 &&
                newI < NUM_ROWS &&
                newK >= 0 &&
                newK < NUM_COLS
              ) {
                neighbors += g[newI][newK];
              }
            });
            if (neighbors < 2 || neighbors > 3) {
              gridCopy[i][k] = 0;
            } else if (g[i][k] === 0 && neighbors === 3) {
              gridCopy[i][k] = 1;
            }
          }
        }
      });
    });

    setTimeout(runSimulation, 200);
  }, []);

  return (
    <>
      <div style={{ textAlign: "center", margin: "16px" }}>
        <Button
          startIcon={running ? <PauseIcon /> : <PlayArrowIcon />}
          variant="outlined"
          size="small"
          onClick={() => {
            setRunning((prev) => !prev);
            if (!running) {
              runningRef.current = true;
              runSimulation();
            }
          }}
          style={{ marginRight: "16px" }}
        >
          {running ? "Stop" : "Start"}
        </Button>

        <Button
          startIcon={<DeleteOutlineOutlinedIcon />}
          variant="outlined"
          size="small"
          onClick={() => {
            setGrid(generateEmptyGrid());
          }}
          style={{ marginRight: "16px" }}
        >
          Clear
        </Button>

        <Button
          startIcon={<ShuffleIcon />}
          variant="outlined"
          size="small"
          onClick={() => {
            setGrid(generateRandomGrid());
          }}
        >
          Random
        </Button>
      </div>

      <div
        style={{
          display: "grid",
          gridTemplateColumns: `repeat(${NUM_COLS}, 20px)`,
          justifyContent: "center",
          color: "#000",
        }}
      >
        {grid.map((rows, i) =>
          rows.map((_, k) => (
            <div
              key={`${i}-${k}`}
              onClick={() => {
                const newGrid = produce(grid, (gridCopy) => {
                  gridCopy[i][k] = grid[i][k] ? 0 : 1;
                });
                setGrid(newGrid);
              }}
              style={{
                width: 20,
                height: 20,
                backgroundColor: grid[i][k] ? "#12a4d9" : undefined,
                borderRadius: "5px",
                border: "solid 1px",
                transition: "all 100ms",
              }}
            ></div>
          ))
        )}
      </div>
    </>
  );
};
