import { AppBar, IconButton, Toolbar, Typography } from "@mui/material";
import GamepadIcon from "@mui/icons-material/Gamepad";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import GitHubIcon from "@mui/icons-material/GitHub";

export const ApplicationBar = (): JSX.Element => {
  return (
    <AppBar position="static" color="primary">
      <Toolbar variant="dense">
        <GamepadIcon style={{ marginRight: "8px" }} />
        <Typography
          variant="h6"
          color="inherit"
          component="div"
          style={{ flexGrow: 1 }}
        >
          Game of Life
        </Typography>

        <IconButton href="https://gitlab.com/George_K" target="_blank">
          <GitHubIcon style={{ color: "white" }} />
        </IconButton>
        <IconButton
          href="https://www.linkedin.com/in/georgekopti/"
          target="_blank"
        >
          <LinkedInIcon style={{ color: "white" }} />
        </IconButton>
      </Toolbar>
    </AppBar>
  );
};
